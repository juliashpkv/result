#pragma once
#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct date
{
    int sec;
    int min;
    int hour;
};

struct person
{
    int num;
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
    char club[MAX_STRING_SIZE];
};

struct book_subscription
{
    person people;
    date start;
    date finish;
};

#endif