#include <iostream>

using namespace std;

#include "book_subscription.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �1\n";
    cout << "�����: ������� ����\n\n";
    cout << "������: 12\n";
    book_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            cout << subscriptions[i]->people.num << '\n';
            cout << subscriptions[i]->people.last_name << '\n';
            cout << subscriptions[i]->people.first_name << '\n';
            cout << subscriptions[i]->people.middle_name << '\n';
            cout << subscriptions[i]->finish.hour << ' ';
            cout << subscriptions[i]->finish.min << ' ';
            cout << subscriptions[i]->finish.sec << '\n';
            cout << subscriptions[i]->start.hour << ' ';
            cout << subscriptions[i]->start.min << ' ';
            cout << subscriptions[i]->start.sec << '\n';
            cout << subscriptions[i]->people.club << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}