#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.min = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.sec = atoi(str_number);
    return result;
}

void read(const char* file_name, book_subscription* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            book_subscription* item = new book_subscription;
            file >> item->people.last_name;
            file >> item->people.first_name;
            file >> item->people.middle_name;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}